package br.ucsal.bes20222.testequalidade.locadora.business;

import org.junit.jupiter.api.Test;

import br.ucsal.bes20222.testequalidade.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.exception.VeiculoNaoEncontradoException;

/**
 * Testes para os métodos da classe LocacaoBO.
 * 
 * @author claudioneiva
 *
 */
public class LocacaoBOTestV3 {

	@Test
	public void exemploUsoDoMasNoTestDataBuilder() throws VeiculoNaoEncontradoException {

		// 1.1. Instanciar 2 veículos, com ano de fabricação 2015; [dado de entrada]
		String placa1 = "ABC-1234";
		String placa2 = "XYZ-7890";
		String placa3 = "XYZ-6782";

		// fabricado em 2015 com diária de 200
		// fabricado em 2020 com diária de 100
		// fabricado em 2021 com diária de 200

		VeiculoBuilder veiculoBuilder = VeiculoBuilder.umVeiculoDisponivel().comDiaria(200d).doModelo("Gol");

		Veiculo veiculo1 = veiculoBuilder.mas().fabricadoEm(2015).comPlaca(placa1).build();
		Veiculo veiculo2 = veiculoBuilder.mas().fabricadoEm(2020).comDiaria(100d).comPlaca(placa2).build();
		Veiculo veiculo3 = veiculoBuilder.mas().fabricadoEm(2021).comPlaca(placa3).build();
	}

}
