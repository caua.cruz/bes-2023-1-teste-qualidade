package br.ucsal.bes20222.testequalidade.locadora.builder;

import java.time.LocalDate;

import br.ucsal.bes20222.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoObjectMother {

	public static Veiculo umVeiculoNovoDisponivel() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca("ABC-34567");
		veiculo.setModelo(new Modelo("Gol"));
		veiculo.setSituacao(SituacaoVeiculoEnum.DISPONIVEL);
		veiculo.setValorDiaria(100d);
		veiculo.setAnoFabricacao(LocalDate.now().minusYears(1).getYear());
		return veiculo;
	}
	
	public static Veiculo umVeiculoAntigoDisponivel() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca("ABC-34567");
		veiculo.setModelo(new Modelo("Gol"));
		veiculo.setSituacao(SituacaoVeiculoEnum.DISPONIVEL);
		veiculo.setValorDiaria(100d);
		veiculo.setAnoFabricacao(2015);
		return veiculo;
	}
	
	public static Veiculo umVeiculoNovoLocado() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca("ABC-34567");
		veiculo.setModelo(new Modelo("Gol"));
		veiculo.setSituacao(SituacaoVeiculoEnum.LOCADO);
		veiculo.setValorDiaria(100d);
		veiculo.setAnoFabricacao(LocalDate.now().minusYears(1).getYear());
		return veiculo;
	}
	
	public static Veiculo umVeiculoAntigoLocado() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca("ABC-34567");
		veiculo.setModelo(new Modelo("Gol"));
		veiculo.setSituacao(SituacaoVeiculoEnum.LOCADO);
		veiculo.setValorDiaria(100d);
		veiculo.setAnoFabricacao(2015);
		return veiculo;
	}
	
}
