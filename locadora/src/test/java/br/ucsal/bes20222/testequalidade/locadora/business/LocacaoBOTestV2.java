package br.ucsal.bes20222.testequalidade.locadora.business;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20222.testequalidade.locadora.builder.VeiculoObjectMother;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;
import br.ucsal.bes20222.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.persistence.VeiculoDAO;

/**
 * Testes para os métodos da classe LocacaoBO.
 * 
 * @author claudioneiva
 *
 */
public class LocacaoBOTestV2 {

	/**
	 * Testar o cálculo do valor total de locação por 4 dias de 2 veículos
	 * fabricados em 2015.
	 * 
	 * Caso de teste:
	 * 
	 * 1. Entrada:
	 * 
	 * a) veículos: 2 fabricados em 2015, com valor diária 100.00
	 * 
	 * b) quantidade de dias: 4
	 * 
	 * 2. Saída esperada:
	 * 
	 * a) valor da locação: 640
	 * 
	 * memória de cálculo (NÃO faz parte do artefato!):
	 * 
	 * 2 * 100 = 200 reais por dia -> 4 x 200 = 800 reais para os 4 dias
	 * 
	 * como os 2 veículos têm mais de 5 anos, o desconto de 20% será sobre toda a
	 * locação
	 * 
	 * 800 * 20% = 160 reais de desconto
	 * 
	 * 800 - 160 = 640 reais valor da locação
	 * 
	 * @throws VeiculoNaoEncontradoException
	 * 
	 */
	@Test
	public void testarCalculoValorTotalLocacao2Veiculos4Dias() throws VeiculoNaoEncontradoException {

		// 1.1. Instanciar 2 veículos, com ano de fabricação 2015; [dado de entrada]
		String placa1 = "ABC-1234";
		String placa2 = "XYZ-7890";
		double diaria = 100d;

		Veiculo veiculo1 = VeiculoObjectMother.umVeiculoAntigoDisponivel();
		veiculo1.setPlaca(placa1);
		veiculo1.setValorDiaria(diaria);

		Veiculo veiculo2 = VeiculoObjectMother.umVeiculoAntigoDisponivel();
		veiculo2.setPlaca(placa2);
		veiculo2.setValorDiaria(diaria);

		// 1.2. Dados de entrada (além dos 2 veículos)
		List<String> placas = Arrays.asList(placa1, placa2);
		Integer qtdDiasLocacao = 4;
		LocalDate dataReferencia = LocalDate.of(2023, 4, 3);

		// 1.3. Calcular o valor esperado da locação para 4 dias dos 2 veículos criados
		// no passo 1; [saída esperada]
		Double valorTotalLocacaoEsperado = 640d;

		// 2. Instanciar o VeiculoDAO
		VeiculoDAO veiculoDAO = new VeiculoDAO();

		// 3. Solicitar ao veiculoDAO instanciado no passo 2 a inclusão dos 2 veículos
		// criados no passo 1;
		veiculoDAO.insert(veiculo1);
		veiculoDAO.insert(veiculo2);

		// 4. Instanciar o LocacaoBO passando como parâmetro o veiculoDAO instanciado no
		// passo 2;
		LocacaoBO locacaoBO = new LocacaoBO(veiculoDAO);

		// 5. Solicitar o cálculo do valor total de locação ao método
		// calcularValorTotalLocacao da instância de LocacaoBO criada no passo 4 e,
		// neste momento, coletar o valor de locação atual;
		Double valorTotalLocacaoAtual = locacaoBO.calcularValorTotalLocacao(placas, qtdDiasLocacao, dataReferencia);

		// 6. Comparar o valor esperado de locação com o valor de locação atual;
		Assertions.assertEquals(valorTotalLocacaoEsperado, valorTotalLocacaoAtual);
	}

}
