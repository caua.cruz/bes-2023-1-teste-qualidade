package br.ucsal._20202.testequalidade.aula03;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.List;

class LinkedListTest {

	private List<String> nomes;

	@BeforeEach
	void setup() {
		nomes = new LinkedList<>();
	}

	/**
	 * Testar a adição de 1 elemento válido à uma lista de Strings.
	 * 
	 * @throws InvalidElementException
	 */
	@Test
	void testarAdd1Elementos() throws InvalidElementException {
		// Dado de entrada
		String nome1 = "antonio";

		// Saída esperada: os nomes na ordem nome1, nome2 e nome4

		// Executar o método que está sendo testado (add)
		nomes.add(nome1);

		// Coletar a saída atual
		String nome1Atual = nomes.get(0);

		// Comparar a saída atual com a saída esperada
		assertEquals(nome1, nome1Atual);

	}

	/**
	 * Testar a adição de 3 elementos válidos à uma lista de Strings.
	 * 
	 * @throws InvalidElementException
	 */
	@Test
	void testarAdd3Elementos() throws InvalidElementException {
		// Dado de entrada
		String nome1 = "antonio";
		String nome2 = "claudio";
		String nome3 = "neiva";

		// Saída esperada: os nomes na ordem nome1, nome2 e nome4

		// Executar o método que está sendo testado (add)
		nomes.add(nome1);
		nomes.add(nome2);
		nomes.add(nome3);

		// Coletar a saída atual
		String nome1Atual = nomes.get(0);
		String nome2Atual = nomes.get(1);
		String nome3Atual = nomes.get(2);

		// Comparar a saída atual com a saída esperada
		assertAll(() -> assertEquals(nome1, nome1Atual), () -> assertEquals(nome2, nome2Atual),
				() -> assertEquals(nome3, nome3Atual));

	}

	/**
	 * Testar a adição de uma elemento não válido (nulo) à lista.
	 */
	@Test
	void testarAddElementoInvalido() {
		// Dado de entrada
		String nomeNull = null;

		// Saída esperada: ocorrêcia da exceção InvalidElementException
		String mensagemEsperada = "The element can't be null.";

		// Executar o método que está sendo testado (add) e coletar a saída atual (a
		// ocorrência exceção)
		InvalidElementException exceptionAtual = Assertions.assertThrows(InvalidElementException.class, () -> nomes.add(nomeNull));
		assertEquals(mensagemEsperada, exceptionAtual.getMessage(), "mensagem enviada na exceção");
	}

}
