package br.ucsal._20202.testequalidade.aula03.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class CalculoUtilTest {

	@Test
	@Disabled
	void testarFatorial0() {
		// Dado de entrada
		int n = 0;

		// Saída esperada
		long fatorialEsperado = 1;

		// Execução do método que está sendo testado e coleta da saída atual
		long fatorialAtual = CalculoUtil.calcularFatorial(n);

		// Comparação da saída esperada com a saída atual
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	@DisplayName("testar calcularFatorial(1)")
	void testarFat1() {
		// Dado de entrada
		int n = 1;

		// Saída esperada
		long fatorialEsperado = 1;

		// Execução do método que está sendo testado e coleta da saída atual
		long fatorialAtual = CalculoUtil.calcularFatorial(n);

		// Comparação da saída esperada com a saída atual
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	void testarFatorial5() {
		int n = 5;
		long fatorialEsperado = 120;
		long fatorialAtual = CalculoUtil.calcularFatorial(n);

		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
