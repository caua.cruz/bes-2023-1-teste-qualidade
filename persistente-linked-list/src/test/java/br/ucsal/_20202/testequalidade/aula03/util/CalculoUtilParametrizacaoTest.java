package br.ucsal._20202.testequalidade.aula03.util;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.MethodSource;

class CalculoUtilParametrizacaoTest {

	// 0 -> 1
	// 1 -> 1
	// 5 -> 120

	@ParameterizedTest(name = "{index}-calcularFatorial({0})={1}")
	@CsvFileSource(resources = "/fatorialCasoTeste.csv", numLinesToSkip = 1)
	//@CsvSource({ "0,1", "1,1", "5,120" })
	// @CsvSource(value = { "0:1", "1:1", "5:120" }, delimiter = ':')
	void testarFatorial(int n, long fatorialEsperado) {
		// Execução do método que está sendo testado e coleta da saída atual
		long fatorialAtual = CalculoUtil.calcularFatorial(n);

		// Comparação da saída esperada com a saída atual
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	
//	@ParameterizedTest
//	@MethodSource("gerarDadosTesteAlocacaoAlunos")
//	void testarAlocacaoAlunos(Aluno aluno, Turma turma, Curso curso, Matricula matricula) {
//	}
//	
//	private static Stream<Arguments> gerarDadosTesteAlocacaoAlunos(){
//		Aluno aluno1 = new Aluno(123,"claudio","1241334");
//		Aluno aluno2 = new Aluno(456,"ana","45756");
//		Aluno aluno3 = new Aluno(678,"clara","235234");
//		Turma turma1 = new Turma(poo, 60, 1);
//		Turma turma2 = new Turma(bd2, 90, 1);
//		Curso curso1 = new Curso("BES".charAt(2400));
//		Matricula matricula1 = new Matricula(aluno1, turma1, curso1);
//		Matricula matricula2 = new Matricula(aluno2, turma1, curso1);
//		return Stream.of(,
//		      Arguments.of(aluno1, turma1, curso1, matricula1),
//		      Arguments.of(aluno1, turma2, curso1, matricula2)
//		    );
//	}
	
}
