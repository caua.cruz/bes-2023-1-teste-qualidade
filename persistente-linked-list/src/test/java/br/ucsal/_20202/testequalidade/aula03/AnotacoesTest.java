package br.ucsal._20202.testequalidade.aula03;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import br.ucsal._20202.testequalidade.aula03.interfaces.List;

@TestInstance(Lifecycle.PER_CLASS)
class AnotacoesTest {

	// @Autowired ou @Inject
	// static AlunoRepository alunoRepository;
	private List<String> nomes;
	
	@BeforeAll
	void setupClass() {
		System.out.println("setupClass...");
		nomes = new LinkedList<>();
	}

	@AfterAll
	void teardownClass() {
		System.out.println("teardownClass...");
	}

	@BeforeEach
	void setup() {
		System.out.println("	setup...");
	}

	@AfterEach
	void teardown() {
		System.out.println("	teardown...");
	}

	@Test
	void test1() {
		System.out.println("		test1...");
	}

	@Test
	@Disabled
	void test2() {
		System.out.println("		test2...");
	}

	@Test
	@DisplayName("Esse vai ser o nome do teste que vai aparecer nos relatórios")
	void test3() {
		Assumptions.assumeTrue(false);
		System.out.println("		test3...");
	}

}
