package br.ucsal._20202.testequalidade.aula03.util;

public class CalculoUtilSemJUnitTest {

	public static int qtdSucesso = 0;
	public static int qtdFalha = 0;

	public static void main(String[] args) {
		testarFatorial0();
		testarFatorial5();
		System.out.println("sucesso=" + qtdSucesso);
		System.out.println("falha=" + qtdFalha);
	}

	private static void testarFatorial0() {
		System.out.print("testarFatorial0 - ");
		int n = 0;
		long fatorialEsperado = 1;
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		if (fatorialEsperado == fatorialAtual) {
			System.out.println("Sucesso");
			qtdSucesso++;
		} else {
			System.out.println("Falha: esperado " + fatorialEsperado + " mas encontrado " + fatorialAtual);
			qtdFalha++;
		}
	}

	private static void testarFatorial5() {
		System.out.print("testarFatorial5 - ");
		int n = 5;
		long fatorialEsperado = 120;
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		if (fatorialEsperado == fatorialAtual) {
			System.out.println("Sucesso");
			qtdSucesso++;
		} else {
			System.out.println("Falha: esperado " + fatorialEsperado + " mas encontrado " + fatorialAtual);
			qtdFalha++;
		}
	}

}
