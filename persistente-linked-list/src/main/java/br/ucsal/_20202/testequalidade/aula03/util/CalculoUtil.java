package br.ucsal._20202.testequalidade.aula03.util;

public class CalculoUtil {

	public static long calcularFatorial(int n) {
		Long fatorial = 1L;
		for (int i = 1; i <= n; i++) {
			fatorial *= i;
		}
		return fatorial;
	}

}
